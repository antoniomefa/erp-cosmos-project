import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from "reactstrap";
import { onLogin } from "../services/login";
import { onRecovery } from "../services/recovery";
import logo from "../assets/img/brand/logo.png";

class Login extends Component {
  state = {
    email: "",
    password: "",
    recovery: false
  };

  onLogin = () => {
    const { email, password } = this.state;
    onLogin(email, password).then(data => {
      this.props.history.push("/");
    });
  };

  onRecovery = () => {
    /* Agregar llamada a la función que envía el correo */
    alert("Se envió un enlace a su correo para restablecer la contraseña.");
    const { email } = this.state;
    onRecovery(email).then(data => {
      this.props.history.push("/");
    });
  };

  disableButton = () => {
    const { email, password } = this.state;
    return password.length === 0 || !this.validateEmail(email);
  };

  validateEmail = email => {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  handleRecovery = () => this.setState({ recovery: !this.state.recovery });

  onChangeValue = (field, { target: { value } }) =>
    this.setState({ [field]: value });

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      {!this.state.recovery ? (
                        <>
                          <h1>Login</h1>
                          <p className="text-muted">
                            Ingrese su nombre de usuario y contraseña
                          </p>
                        </>
                      ) : (
                        <>
                          <h1>Recuperar contraseña</h1>
                          <p className="text-muted">
                            Ingrese su correo electrónico
                          </p>
                        </>
                      )}
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="email"
                          placeholder="Email"
                          value={this.state.email}
                          onChange={this.onChangeValue.bind(this, "email")}
                          autoComplete="password"
                        />
                      </InputGroup>

                      {!this.state.recovery && (
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input
                            type="password"
                            placeholder="Password"
                            autoComplete="current-password"
                            value={this.state.password}
                            onChange={this.onChangeValue.bind(this, "password")}
                          />
                        </InputGroup>
                      )}

                      <Row>
                        <Col xs="6">
                          <Button
                            onClick={
                              !this.state.recovery
                                ? this.onLogin
                                : this.onRecovery
                            }
                            /* disabled={this.disableButton()} */ color="primary"
                            className="px-4"
                          >
                            {!this.state.recovery ? "Login" : "Enviar"}
                          </Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button
                            color="link"
                            className="px-0"
                            onClick={() => this.handleRecovery()}
                          >
                            {this.state.recovery
                              ? "Login"
                              : "Olvidé mi contraseña"}
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: "44%" }}
                >
                  <CardBody className="text-center">
                    <div>
                      <img src={logo} />
                      <h2>SINDICATO NACIONAL PRIMERO DE MAYO</h2>
                      <p>
                        El respeto a los derechos de los trabajadores y el
                        bienestar de sus familias son esenciales para el
                        desarrollo de cualquier sociedad.
                      </p>
                      <a href="http://sn1m.org.mx/contacto/" target="_blank">
                        <Button
                          color="primary"
                          className="mt-3"
                          active
                          tabIndex={-1}
                        >
                          Contactar!
                        </Button>
                      </a>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
