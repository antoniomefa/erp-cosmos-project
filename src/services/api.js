export default {
  login: "api/v1/users/login",
  //empresas: 'Seguridad/Empresa/Listado',

  cuentasUsuarios: "wsuser/srv_fUserData",

  //*******   Apis de Cosmos   ********//

  ejercicios: "Presupuestos/EjercicioOperativo/Listado",
  guardarEjercicio: "Presupuestos/EjercicioOperativo/Guardar",
  eliminarEjercicio: "Presupuestos/EjercicioOperativo/Eliminar",

  periodos: "Presupuestos/PeriodoOperativo/Listado",
  guardarPeriodo: "Presupuestos/PeriodoOperativo/Guardar",
  eliminarPeriodo: "Presupuestos/PeriodoOperativo/Eliminar",

  titulos: "Presupuestos/TituloRubro/Listado",
  guardarTitulo: "Presupuestos/TituloRubro/Guardar",
  eliminarTitulo: "Presupuestos/TituloRubro/Eliminar",

  rubros: "Presupuestos/RubroContable/Listado",
  guardarRubro: "Presupuestos/RubroContable/Guardar",
  eliminarRubro: "Presupuestos/RubroContable/Eliminar",

  centros: "Compras/CentroCosto/Listado",
  guardarCentro: "Compras/CentroCosto/Guardar",
  eliminarCentro: "Compras/CentroCosto/Eliminar",

  conceptoIngresos: "Compras/ConceptoIngreso/Listado",
  guardarConceptoIngreso: "Compras/ConceptoIngreso/Guardar",
  eliminarIngreso: "Compras/ConceptoIngreso/Eliminar",

  conceptoEgresos: "Compras/ConceptoEgreso/Listado",
  guardarConceptoEgreso: "Compras/ConceptoEgreso/Guardar",
  eliminarEgreso: "Compras/ConceptoEgreso/Eliminar",

  cuentasContables: "Compras/CuentaContable/Listado",
  guardarCuentaContable: "Compras/CuentaContable/Guardar",
  eliminarCuenta: "Compras/CuentaContable/Eliminar",

  presupuestoSolicitado: "Presupuestos/Solicitado",
  GuardarPptoPerEgr: "Presupuestos/PptoPerEgr/Guardar",
  GuardarPptoDesgEgr: "Presupuestos/PptoDesgEgr/Guardar",
  GuardarPptoConceptoEgr: "Presupuestos/PptoConcepto/Guardar",
  encabezadoEgresos: "Presupuestos/PptoEncEgr/Listado",
  guardarEncabezadoEgresos: "Presupuestos/PptoEncEgr/Guardar",
  guardarDetalleEgresos: "Presupuestos/PptoDetEgr/Guardar",

  encabezadoIngresos: "Presupuestos/PptoEncIng/Listado",
  guardarEncabezadoIngresos: "Presupuestos/PptoEncIng/Guardar",
  guardarDetalleIngresos: "Presupuestos/PptoDetIng/Guardar"
};
