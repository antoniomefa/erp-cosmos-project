import Service from '../service';
import Api from '../api';

export const getCuentas = () => {
    const data = cuentasData();
    const options = {body: JSON.stringify(data)};
    return Service(Api.cuentasUsuarios, 'POST', options);
}

export const crearCuenta = cuenta => {
    const data = cuentaData(cuenta.CuentaContableClave, cuenta.Nombre);
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarCuentaContable, 'POST', options);
}

export const actualizarCuenta = cuenta => {
    const data = cuentaData();
    data.Datos = cuenta;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarCuentaContable, 'POST', options);
};

const cuentasData = () => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {

    },
    "Parametros": []
});

const cuentaData = (key = 0, name = '') => ({
    "UsuarioID": 2,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"CuentaContableClave": key,
    	"Nombre": name
    },
    "Parametros": []
});
