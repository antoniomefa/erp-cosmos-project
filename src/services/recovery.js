import Service from './service';
import Api from './api';

export const onRecovery = (email) => {
    const data = getDataRecovery(email);
    const options = {body: JSON.stringify(data)};
    return Service(Api.recovery, 'POST', options); /* Agregar el Api que recibe el request para el recovery */
}

const getDataRecovery = (email) => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": "",
    "Parametros": [
        {
            "nombre": "CorreoElectronico",
            "valor": email
        },
        {
            "nombre": "NombreHost",
            "valor": "Xps13"
        },
        {
            "nombre": "Utc",
            "valor": "-5"
        }
    ]
})
