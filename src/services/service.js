//import Session from "../helpers/session";

const service = (
  path,
  method = "GET",
  options,
  baseUrl = null,
  isTextResponse = false
) => {
  var opts = {
    "Content-Type": "application/json"
  };

  /* const session = Session.shared();                  // Suprimido para la peticion a somos-sn1m
    if (session.isLoguedIn()) {
        opts['Authorization'] = session._auth;
    } */

  // TODO agregar query string
  const url = baseUrl ? baseUrl : "https://secret-eyrie-37900.herokuapp.com";

  return fetch(`${url}/${path}`, {
    method,
    headers: { ...opts },
    ...options
  })
    .then(response => {
      const OK = 200;
      const ERROR = 500;
      const UNAUTH = 401;

      if (response.status !== OK) {
        if (response.status === UNAUTH) {
          throw Error("unauthorized");
        } else if (response.status === ERROR) {
          throw Error("server error");
        } else {
          return response.text().then(error => {
            throw Error(error);
          });
        }
      }

      // const token = response.headers.get("Access-Token");
      // if (token) {
      //   Session.shared().createSession(token);
      // }

      return isTextResponse ? response.text() : response.json();
    })
    .catch(err => {
      try {
        let response = JSON.parse(err.message);
        if (response.error) {
          throw Error(response.error);
        } else if (typeof response == "object") {
          throw Error(err.message);
        }

        throw Error("Error inesperado.");
      } catch {
        if (err.message === "unauthorized") {
          throw Error("Usuario no autorizado.");
        } else if (err.message === "server error") {
          throw Error("Error inesperado.");
        }

        let response = JSON.parse(err.message);
        if (response) {
          throw Error(err.message);
        }
        throw Error("Error inesperado.");
      }
    });
};

export default service;
