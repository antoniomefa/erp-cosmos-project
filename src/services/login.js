import Service from "./service";
import Api from "./api";
import Session from "../helpers/session";

export const onLogin = async (email, password) => {
  const data = JSON.stringify({ email, password });
  const options = { body: data };
  const response = await Service(Api.login, "POST", options);

  Session.shared().createSession(response.token);

  return response;
};
