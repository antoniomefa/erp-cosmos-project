import Service from '../service';
import Api from '../api';

export const getEjercicios = () => {
    return Service(Api.ejercicios, 'POST');
}

export const crearEjercicio = ejercicio => {
    const data = ejercicioData(ejercicio.EjercicioClave, ejercicio.Nombre, ejercicio.NombreCorto);
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarEjercicio, 'POST', options);
}

export const actualizarEjercicio = ejercicio => {
    const data = ejercicioData();
    data.Datos = ejercicio;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarEjercicio, 'POST', options);
};

const ejercicioData = (key = 0, name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EjercicioClave": key,
    	"Nombre": name,
    	"NombreCorto": shortName
    },
    "Parametros": []
});
