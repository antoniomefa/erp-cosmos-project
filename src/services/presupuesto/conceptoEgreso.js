import Service from '../service';
import Api from '../api';

export const getConceptos = () => {
    const data = conceptosData();
    const options = {body: JSON.stringify(data)};
    return Service(Api.conceptoEgresos, 'POST', options);
}

export const crearConcepto = concepto => {
    const data = conceptoData(
        concepto.ConceptoEgresoClave,
        concepto.Nombre,
        concepto.NombreCorto
    );
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarConceptoEgreso, 'POST', options);
}

export const actualizarConcepto = concepto => {
    const data = conceptoData();
    data.Datos = concepto;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarConceptoEgreso, 'POST', options);
};

const conceptosData = () => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1
    },
    "Parametros": []
});

const conceptoData = (key = '', name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"ConceptoEgresoClave": key,
    	"Nombre": name,
    	"NombreCorto": shortName,
    	"CompraFactura": "x",
    	"Desglosar": "x",
    	"EmpresaID": 1
    },
    "Parametros": []
});
