import Service from '../service';
import Api from '../api';
import moment from 'moment';

export const getPeriodos = () => {
    return Service(Api.periodos, 'POST');
}

export const crearPeriodo = (ejercicioId, periodo) => {
    const data = periodoData(
        ejercicioId,
        periodo.PeriodoClave,
        periodo.Nombre,
        periodo.NombreCorto,
        periodo.PeriodoOrden,
        moment(periodo.FechaInicial).format('DD/MM/YYYY'),
        moment(periodo.FechaFinal).format('DD/MM/YYYY'),
        moment(periodo.FechaEjercePresupuesto).format('DD/MM/YYYY')
    );
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarPeriodo, 'POST', options);
}

export const actualizarPeriodo = periodo => {
    const data = periodoData();
    data.Datos = periodo;

    data.Datos.FechaInicial = moment(data.Datos.FechaInicial).format('DD/MM/YYYY');
    data.Datos.FechaFinal = moment(data.Datos.FechaFinal).format('DD/MM/YYYY');
    data.Datos.FechaEjercePresupuesto = moment(data.Datos.FechaEjercePresupuesto).format('DD/MM/YYYY');

    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarPeriodo, 'POST', options);
};

const periodoData = (ejercicio = 0, key = 0, name = '', shortName = '', sort = 0, initialDate = '', finalDate = '', budgetDate = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EjercicioOperativoID": ejercicio,
    	"Nombre": name,
    	"NombreCorto": shortName,
    	"PeriodoClave": key,
    	"PeriodoOrden": sort,
    	"FechaInicial": initialDate,
    	"FechaFinal": finalDate,
    	"FechaEjercePresupuesto": budgetDate
    },
    "Parametros": []
});
