import Service from '../service';
import Api from '../api';

export const getConceptos = (concept, exerciseId) => {
    const data = conceptosData();
    const options = {body: JSON.stringify(data)};
    return Service(Api.conceptoIngresos, 'POST', options);
}

export const crearConcepto = concepto => {
    const data = conceptoData(
        concepto.Nombre,
        concepto.NombreCorto
    );
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarConceptoIngreso, 'POST', options);
}

export const actualizarConcepto = concepto => {
    const data = conceptoData();
    data.Datos = concepto;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarConceptoIngreso, 'POST', options);
};

const conceptosData = () => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1
    },
    "Parametros": []
});

const conceptoData = (name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"Nombre": name,
    	"NombreCorto": shortName,
    	"CompraFactura": "x",
    	"Desglosar": "x",
    	"EmpresaID": 1
    },
    "Parametros": []
});
