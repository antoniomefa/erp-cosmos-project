import Service from '../service';
import Api from '../api';

export const getCentros = (concept, exerciseId) => {
    const data = centrosData(concept, exerciseId);
    const options = {body: JSON.stringify(data)};
    return Service(Api.centros, 'POST', options);
}

export const crearCentro = centro => {
    const data = centroData(
        centro.CentroCostoClave,
        centro.Nombre,
        centro.NombreCorto
    );
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarCentro, 'POST', options);
}

export const actualizarCentro = centro => {
    const data = centroData();
    data.Datos = centro;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarCentro, 'POST', options);
};

const centrosData = () => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1
    },
    "Parametros": []
});

const centroData = (key = '', name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1,
    	"CentroCostoClave": key,
    	"Nombre": name,
    	"NombreCorto": shortName,
    	"Administracion": 1
    },
    "Parametros": []
});
