import Service from '../service';
import Api from '../api';

export const getTitulos = (concept, exerciseId) => {
    const data = titulosData(concept, exerciseId);
    const options = {body: JSON.stringify(data)};
    return Service(Api.titulos, 'POST', options);
}

export const crearTitulo = (exerciseId, concept, titulo) => {
    const data = tituloData(
        concept,
        exerciseId,
        titulo.TituloRubroClave,
        titulo.Nombre,
        titulo.NombreCorto
    );
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarTitulo, 'POST', options);
}

export const actualizarTitulo = titulo => {
    const data = tituloData();
    data.Datos = titulo;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarTitulo, 'POST', options);
};

const titulosData = (concept = 'I', exercise = 1) => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1,
    	"IngresoOEgreso": concept,
    	"EjercicioOperativoID": exercise
    },
    "Parametros": []
});

const tituloData = (concept = 'I', exercise = 1, key = '', name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"EmpresaID": 1,
    	"IngresoOEgreso": concept,
    	"EjercicioOperativoID": exercise,
    	"TituloRubroClave": key,
    	"Nombre": name,
    	"NombreCorto": shortName,
    	"CalculoRemanente": true
    },
    "Parametros": []
});
