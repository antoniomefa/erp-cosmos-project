import Service from '../service';
import Api from '../api';

export const getRubros = titleId => {
    const data = rubrosData(titleId);
    const options = {body: JSON.stringify(data)};
    return Service(Api.rubros, 'POST', options);
}

export const crearRubro = (titleId, rubro) => {
    const data = rubroData(titleId, rubro.Nombre, rubro.NombreCorto);
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarRubro, 'POST', options);
}

export const actualizarRubro = rubro => {
    const data = rubroData();
    data.Datos = rubro;
    const options = {body: JSON.stringify(data)};
    return Service(Api.guardarRubro, 'POST', options);
};

const rubrosData = titleId => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"TituloRubroID": titleId
    },
    "Parametros": []
})

const rubroData = (titleId = 1, name = '', shortName = '') => ({
    "UsuarioID": 0,
    "EmpresaID": 0,
    "SesionID": "",
    "Datos": {
    	"TituloRubroID": titleId,
    	"Nombre": name,
    	"NombreCorto": shortName
    },
    "Parametros": []
});
