import React from 'react';
import TableCollapse from '../../../components/tables/collapse';
import {getCuentas, crearCuenta, actualizarCuenta} from '../../../services/presupuesto/cuentasContables';
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Col,
    FormGroup,
    Label,
    Input,
    Row,
 } from 'reactstrap';

 import fondo from '../../../assets/img/administrador.png' //*** Eliminar */

class EjerciciosYPeriodos extends React.Component {
    state = {
        modalOpen: false,
        dataObject: {},
        cuentas: [],
    }

    componentDidMount = () => {
        this.fetchData();
    }

    fetchData = () => {
        getCuentas()
            .then(data => {
                this.setState({
                    cuentas: data.Datos
                });
            });
    };

    toggleModal = field => {
        this.setState({[field]: !this.state[field]});
    }

    agregarCuenta = () => this.setState({
        modalOpen: true,
        isCreating: true,
    });

    editarCuenta = row => {
        this.setState({
            modalOpen: true,
            dataObject: row,
            isCreating: false
        });
    };

    guardarCuenta = () => {
        const {isCreating, dataObject} = this.state;

        if (isCreating) {
            crearCuenta(dataObject)
                .then(data => {
                    const cuentas = [...this.state.cuentas];
                    cuentas.push(data.Datos);
                    this.setState({
                        cuentas,
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
                .catch(err => {
                    console.log('err', err);
                })
        } else {
            actualizarCuenta(dataObject)
                .then(data => {
                    this.setState({
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
        }
    }

    onChangeDataObject = (field, {target: {value}}) => {
        const dataObject = {...this.state.dataObject};
        dataObject[field] = value;
        this.setState({dataObject});
    };

    renderModal = () => (
        <Modal isOpen={this.state.modalOpen} toggle={this.toggleModal.bind(this, 'modalOpen')}>
            <ModalHeader toggle={this.toggle}>{this.state.isCreating ? 'Creando Cuenta contable' : 'Editando cuenta contable'}</ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Clave</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.CuentaContableClave}
                        onChange={this.onChangeDataObject.bind(this, 'CuentaContableClave')}
                        placeholder="Clave"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarCuenta}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    addButtonLancamento = (cell, row) => {
        return (
            <Button
                outline
                color="primary"
                onClick={this.editarCuenta.bind(this, row)}>
                <span className="fa fa-edit" />
            </Button>
        )
    }

    obtenerTituloColumnas = () => [
        {dataField: 'CuentaContableID', text: 'ID'},
        {dataField: 'Nombre', text: 'Nombre'},
        {dataField: 'CuentaContableClave', text: 'Clave'},
        {
            text: '',
            formatter: this.addButtonLancamento,
            editable: false
        }
    ];

    render = () => (
        <>
        
            <img src={fondo}/>
        
            {/* <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 5}}>
                <Button
                    outline
                    color="primary"
                    onClick={this.agregarCuenta}>
                    <span className="fa fa-plus" />
                </Button>
            </div>
            <TableCollapse
                data={this.state.cuentas}
                columns={this.obtenerTituloColumnas()}
                keyField={'CuentaContableID'}
            />
            {this.renderModal()} */}
        </>
    );
}




export default EjerciciosYPeriodos;
