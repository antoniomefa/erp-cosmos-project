import React from "react";
import TableCollapse from "../../../components/tables/collapse";
import {
  getCuentas,
  crearCuenta,
  actualizarCuenta
} from "../../../services/usuarios/cuentasUsuarios";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Col,
  FormGroup,
  Label,
  Input,
  Row
} from "reactstrap";

class CuentasUsuarios extends React.Component {
  state = {
    modalOpen: false,
    confirmModal: false,
    dataObject: {},
    cuentas: []
  };

  componentDidMount = () => {
    this.fetchData();
  };

  fetchData = () => {
    getCuentas()
      .then(data => {
        //console.log("Leído del API")
        let fecha = new Date();
        localStorage.setItem("lastTime", fecha.toLocaleString());
        localStorage.setItem(
          "localUserData",
          JSON.stringify(data[0].oData)
        ); /* Almacena en LocalStorage los datos de la consulta */
        this.setState({
          cuentas: data[0].oData
        });
      })
      .catch(error => {
        //console.log("Leído de LocalStorage")
        this.setState({
          cuentas: JSON.parse(
            localStorage.getItem("localUserData")
          ) /* Recupera la información almacenada de la última consulta */
        });
      });
  };

  toggleModal = field => {
    this.setState({ [field]: !this.state[field] });
  };

  agregarUsuario = () =>
    this.setState({
      modalOpen: true,
      isCreating: true
    });

  editarCuenta = row => {
    this.setState({
      modalOpen: true,
      dataObject: row,
      isCreating: false
    });
  };

  guardarCuenta = () => {
    const { isCreating, dataObject } = this.state;
    debugger;
    if (isCreating) {
      crearCuenta(dataObject)
        .then(data => {
          const cuentas = [...this.state.cuentas];
          cuentas.push(data.Datos);
          this.setState(
            {
              cuentas,
              modalOpen: false,
              confirmModal: false,
              dataObject: {},
              isCreating: false
            },
            this.fetchData
          );
        })
        .catch(err => {
          console.log("err", err);
        });
    } else {
      actualizarCuenta(dataObject).then(data => {
        this.setState(
          {
            modalOpen: false,
            confirmModal: false,
            dataObject: {},
            isCreating: false
          },
          this.fetchData
        );
      });
    }
  };

  onChangeDataObject = (field, { target: { value } }) => {
    const dataObject = { ...this.state.dataObject };
    dataObject[field] = value;
    this.setState({ dataObject });
  };

  renderModal = () => (
    <Modal
      isOpen={this.state.modalOpen}
      toggle={this.toggleModal.bind(this, "modalOpen")}
    >
      <ModalHeader toggle={this.toggle}>
        {this.state.isCreating ? "Nuevo usuario" : "Editando usuario"}
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="12">
            <FormGroup>
              <Label htmlFor="name">Nombre</Label>
              <Input
                type="text"
                id="name"
                value={this.state.dataObject.Nombre}
                onChange={this.onChangeDataObject.bind(this, "Nombre")}
                placeholder="nombre apellidos"
                required
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="email">Correo electrónico</Label>
              <Input
                type="text"
                id="email"
                value={this.state.dataObject.CuentaContableClave}
                onChange={this.onChangeDataObject.bind(
                  this,
                  "CuentaContableClave"
                )}
                placeholder="usuario@correo.com"
                required
              />
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="password">Contraseña</Label>
              <Input
                type="password"
                id="password-input"
                value={this.state.dataObject.Password}
                onChange={this.onChangeDataObject.bind(this, "Password")}
                required
              />
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="phone">Teléfono</Label>
              <Input
                type="text"
                id="phone-input"
                value={this.state.dataObject.Telefono}
                onChange={this.onChangeDataObject.bind(this, "Telefono")}
                placeholder="xxx-xx-xxx-xx"
                required
              />
            </FormGroup>
          </Col>
          <Row>
            <Col xs="6">
              <FormGroup>
                <Label htmlFor="status">Estatus</Label>
                <div className="custom-control custom-switch">
                  <input
                    type="checkbox"
                    class="custom-control-input"
                    id="customSwitch1"
                  />
                  <label className="custom-control-label" for="customSwitch1">
                    Activo
                  </label>
                </div>
              </FormGroup>
            </Col>
          </Row>
        </Row>
        <Row>
          <Col>
            <Label htmlFor="roles">Roles</Label>
          </Col>
        </Row>
        <Row>
          <Col sm="6">
            <FormGroup check>
              <Label check>
                <Input type="checkbox" /> Trabajador
              </Label>
              <br />
              <Label check>
                <Input type="checkbox" /> Delegado en Zona
              </Label>
              <Label check>
                <Input type="checkbox" /> Autorizador de préstamos
              </Label>
            </FormGroup>
          </Col>
          <Col sm="6">
            <Label check>
              <Input type="checkbox" /> Alta Dirección
            </Label>
            <Label check>
              <Input type="checkbox" /> Administrador del Sistema
            </Label>
            <Label check>
              <Input type="checkbox" /> Administrador de la Caja de Ahorro
            </Label>
          </Col>
          {/* <Input
                type="text"
                id="roles"
                value={this.state.dataObject.CuentaContableClave}
                onChange={this.onChangeDataObject.bind(this, "AltaFecha")}
                placeholder="Ej.  Administrador, Trabajador"
                required
              ></Input> */}
        </Row>
        <Modal
          isOpen={this.state.confirmModal}
          toggle={this.toggleModal.bind(this, "confirmModal")}
        >
          <ModalHeader>Advertencia!</ModalHeader>
          <ModalBody>
            {this.state.isCreating
              ? "Está seguro agregar el usuario?"
              : "Está seguro de guardar los cambios?"}
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.guardarCuenta}>
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={this.toggleModal.bind(this, "confirmModal")}
            >
              Regresar
            </Button>
          </ModalFooter>
        </Modal>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          onClick={this.toggleModal.bind(this, "confirmModal")}
        >
          Guardar
        </Button>{" "}
        <Button
          color="secondary"
          onClick={this.toggleModal.bind(this, "modalOpen")}
        >
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  addButtonLancamento = (cell, row) => {
    return (
      <>
        <Button
          outline
          color="primary"
          onClick={this.editarCuenta.bind(this, row)}
        >
          <span className="fa fa-edit" />
        </Button>
      </>
    );
  };

  obtenerTituloColumnas = () => [
    { dataField: "userID", text: "ID" },
    { dataField: "username", text: "Nombre" },
    { dataField: "email", text: "Correo" },
    { dataField: "UserTypeID", text: "Roles" },
    { dataField: "active", text: "Estatus" },
    {
      text: "Acciones",
      formatter: this.addButtonLancamento,
      editable: false,
      csvExport: false
    }
  ];

  render = () => (
    <>
      <h3>Cuentas de usuarios</h3>
      <div
        style={{ display: "flex", justifyContent: "flex-end", marginBottom: 5 }}
      >
        <Button outline color="primary" onClick={this.agregarUsuario}>
          <span className="fa fa-user-plus" />
        </Button>
      </div>
      <TableCollapse
        data={this.state.cuentas}
        columns={this.obtenerTituloColumnas()}
        keyField={"userID"}
      />
      {this.renderModal()}
      <p>Basado en información del {localStorage.lastTime}</p>
    </>
  );
}

export default CuentasUsuarios;
