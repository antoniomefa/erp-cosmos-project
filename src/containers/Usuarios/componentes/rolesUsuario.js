import React from "react";
import TableCollapse from "../../../components/tables/collapse";
import {
  getCuentas,
  crearCuenta,
  actualizarCuenta
} from "../../../services/presupuesto/cuentasContables";
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Col,
  FormGroup,
  Label,
  Input,
  Row,
  CustomInput
} from "reactstrap";

class RolesPorUsuario extends React.Component {
  state = {
    modalOpen: false,
    confirmModal: false,
    dataObject: {},
    cuentas: []
  };

  componentDidMount = () => {
    this.fetchData();
  };

  fetchData = () => {
    getCuentas()
      .then(data => {
        //console.log("Leído del API")
        let fecha = new Date();
        localStorage.setItem("lastTime", fecha.toLocaleString());
        localStorage.setItem(
          "localUserData",
          JSON.stringify(data.Datos)
        ); /* Almacena en LocalStorage los datos de la consulta */
        this.setState({
          cuentas: data.Datos
        });
      })
      .catch(error => {
        //console.log("Leído de LocalStorage")
        this.setState({
          cuentas: JSON.parse(
            localStorage.getItem("localUserData")
          ) /* Recupera la información almacenada de la última consulta */
        });
      });
  };

  toggleModal = field => {
    this.setState({ [field]: !this.state[field] });
  };

  /* agregarUsuario = () =>
    this.setState({
      modalOpen: true,
      isCreating: true
    }); */

  editarRoles = row => {
    //debugger;
    this.setState({
      modalOpen: true,
      dataObject: row
      //isCreating: false
    });
  };

  guardarCuenta = () => {
    const { isCreating, dataObject } = this.state;

    /* if (isCreating) {
      crearCuenta(dataObject)
        .then(data => {
          const cuentas = [...this.state.cuentas];
          cuentas.push(data.Datos);
          this.setState(
            {
              cuentas,
              modalOpen: false,
              confirmModal: false,
              dataObject: {},
              isCreating: false
            },
            this.fetchData
          );
        })
        .catch(err => {
          console.log("err", err);
        });
    } else { */
    actualizarCuenta(dataObject).then(data => {
      this.setState(
        {
          modalOpen: false,
          confirmModal: false,
          dataObject: {},
          isCreating: false
        },
        this.fetchData
      );
    });
    //}
  };

  obtenerRoles = () => [
    "Administrador del Sistema",
    "Administrador de Caja de Ahorros",
    "Autorizador de Préstamos",
    "Delegado en Zona",
    "Alta Dirección",
    "Trabajador"
  ];

  onChangeDataObject = (field, { target: { value } }) => {
    const dataObject = { ...this.state.dataObject };
    dataObject[field] = value;
    this.setState({ dataObject });
  };

  renderModal = () => (
    <Modal
      isOpen={this.state.modalOpen}
      toggle={this.toggleModal.bind(this, "modalOpen")}
    >
      <ModalHeader toggle={this.toggle}>
        Editando roles de {this.state.dataObject.username}
      </ModalHeader>
      <ModalBody>
        <Row>
          <Col xs="6">
            <FormGroup>
              <Label for="exampleCustomMutlipleSelect">
                Roles disponibles:
              </Label>
              <CustomInput
                type="select"
                id="exampleCustomMutlipleSelect"
                name="customSelect"
                multiple
              >
                {this.obtenerRoles().map(rol => (
                  <option key={rol} value={rol}>
                    {rol}
                  </option>
                ))}
              </CustomInput>
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label for="exampleCustomMutlipleSelect">Roles asignados:</Label>
              <CustomInput
                type="select"
                id="exampleCustomMutlipleSelect"
                name="customSelect"
                multiple
              >
                {this.obtenerRoles().map(rol => (
                  <option key={rol} value={rol}>
                    {rol}
                  </option>
                ))}
              </CustomInput>
            </FormGroup>
          </Col>
        </Row>

        <Row>
          <Col sm={{ size: "auto", offset: 2 }}>
            <Button color="info" size="sm">
              Agregar >>
            </Button>{" "}
          </Col>
          <Col sm={{ size: "auto", offset: 3 }}>
            <Button color="info" size="sm">
              {"<< Quitar"}
            </Button>
          </Col>
        </Row>

        <Modal
          isOpen={this.state.confirmModal}
          toggle={this.toggleModal.bind(this, "confirmModal")}
        >
          <ModalHeader>Advertencia!</ModalHeader>
          <ModalBody>Está seguro de guardar los cambios?</ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.guardarCuenta}>
              Confirmar
            </Button>{" "}
            <Button
              color="secondary"
              onClick={this.toggleModal.bind(this, "confirmModal")}
            >
              Regresar
            </Button>
          </ModalFooter>
        </Modal>
      </ModalBody>
      <ModalFooter>
        <Button
          color="primary"
          onClick={this.toggleModal.bind(this, "confirmModal")}
        >
          Guardar
        </Button>{" "}
        <Button
          color="secondary"
          onClick={this.toggleModal.bind(this, "modalOpen")}
        >
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );

  addButtonLancamento = (cell, row) => {
    return (
      <>
        <Button
          outline
          color="primary"
          onClick={this.editarRoles.bind(this, row)}
        >
          <span className="fa fa-key" />
        </Button>
      </>
    );
  };

  obtenerTituloColumnas = () => [
    { dataField: "userID", text: "ID" },
    { dataField: "username", text: "Usuario" },
    { dataField: "UserTypeID", text: "Roles" },
    { dataField: "active", text: "Estatus" },
    {
      text: "Acciones",
      formatter: this.addButtonLancamento,
      editable: false,
      csvExport: false
    }
  ];

  render = () => (
    <>
      <h3>Roles por usuario</h3>
      {/* <div
        style={{ display: "flex", justifyContent: "flex-end", marginBottom: 5 }}
      >
        <Button outline color="primary" onClick={this.agregarUsuario}>
          <span className="fa fa-user-plus" />
        </Button>
      </div> */}
      <TableCollapse
        data={this.state.cuentas}
        columns={this.obtenerTituloColumnas()}
        keyField={"CuentaContableID"}
      />
      {this.renderModal()}
      <p>Basado en información del {localStorage.lastTime}</p>
    </>
  );
}

export default RolesPorUsuario;
