import React from 'react';
import TableCollapse from '../../../components/tables/collapse';
import {getCentros, crearCentro, actualizarCentro} from '../../../services/presupuesto/centroCostos';
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Col,
    FormGroup,
    Label,
    Input,
    Row,
 } from 'reactstrap';

class EjerciciosYPeriodos extends React.Component {
    state = {
        modalOpen: false,
        dataObject: {},
        centros: [],
    }

    componentDidMount = () => {
        this.fetchData();
    }

    fetchData = () => {
        getCentros()
            .then(data => {
                this.setState({
                    centros: data.Datos
                });
            });
    };

    toggleModal = field => {
        this.setState({[field]: !this.state[field]});
    }

    agregarCentro = () => this.setState({
        modalOpen: true,
        isCreating: true,
    });

    agregarRubro = titleId => this.setState({
        modalRubroOpen: true,
        isCreating: true,
        titleId,
    });

    editarCentro = row => {
        this.setState({
            modalOpen: true,
            dataObject: row,
            isCreating: false
        });
    };

    guardarCentro = () => {
        const {isCreating, dataObject} = this.state;

        if (isCreating) {
            crearCentro(dataObject)
                .then(data => {
                    const centros = [...this.state.centros];
                    centros.push(data.Datos);
                    this.setState({
                        centros,
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
                .catch(err => {
                    console.log('err', err);
                })
        } else {
            actualizarCentro(dataObject)
                .then(data => {
                    this.setState({
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
        }
    }

    onChangeDataObject = (field, {target: {value}}) => {
        const dataObject = {...this.state.dataObject};
        dataObject[field] = value;
        this.setState({dataObject});
    };

    renderModal = () => (
        <Modal isOpen={this.state.modalOpen} toggle={this.toggleModal.bind(this, 'modalOpen')}>
            <ModalHeader toggle={this.toggle}>{this.state.isCreating ? 'Creando Centro de costo' : 'Editando centro de costo'}</ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre corto</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.NombreCorto}
                        onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                        placeholder="nombre corto"
                        required
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Clave</Label>
                      <Input
                        type="text"
                        value={this.state.dataObject.CentroCostoClave}
                        onChange={this.onChangeDataObject.bind(this, 'CentroCostoClave')}
                        id="name"
                        placeholder="clave"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarCentro}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    addButtonLancamento = (cell, row) => {
        return (
            <Button
                outline
                color="primary"
                onClick={this.editarCentro.bind(this, row)}>
                <span className="fa fa-edit" />
            </Button>
        )
    }

    obtenerTituloColumnas = () => [
        {dataField: 'CentroCostoID', text: 'ID'},
        {dataField: 'CentroCostoClave', text: 'Clave'},
        {dataField: 'Nombre', text: 'Nombre'},
        {dataField: 'NombreCorto', text: 'Nombre Corto'},
        {
            text: '',
            formatter: this.addButtonLancamento,
            editable: false
        }
    ];

    render = () => (
        <>
            <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 5}}>
                <Button
                    outline
                    color="primary"
                    onClick={this.agregarCentro}>
                    <span className="fa fa-plus" />
                </Button>
            </div>
            <TableCollapse
                data={this.state.centros}
                columns={this.obtenerTituloColumnas()}
                keyField={'CentroCostoID'}
            />
            {this.renderModal()}
        </>
    );
}




export default EjerciciosYPeriodos;
