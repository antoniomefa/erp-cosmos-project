import React from 'react';
import TableCollapse from '../../../components/tables/collapse';
import DatePicker from '../../../components/inputs/datePicker';
// import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import {getEjercicios, crearEjercicio, actualizarEjercicio} from '../../../services/presupuesto/ejercicios';
import {crearPeriodo, actualizarPeriodo} from '../../../services/presupuesto/periodos';
import {
    Badge,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Col,
    FormGroup,
    Label,
    Input,
    Row,
 } from 'reactstrap';

class EjerciciosYPeriodos extends React.Component {
    state = {
        modalEjercicioOpen: false,
        modalPeriodoOpen: false,
        ejercicioId: null,
        dataObject: {},
        ejercicios: [],
    }

    componentDidMount = () => {
        this.fetchData();
    }

    fetchData = () => {
        getEjercicios()
            .then(data => this.setState({ejercicios: data.Datos}));
    }

    toggleModal = field => {
        this.setState({[field]: !this.state[field]});
    }

    agregarEjercicio = () => this.setState({
        modalEjercicioOpen: true,
        isCreating: true,
    });

    agregarPeriodo = ejercicioId => this.setState({
        modalPeriodoOpen: true,
        isCreating: true,
        ejercicioId,
    });

    editarEjercicio = row => {
        this.setState({
            modalEjercicioOpen: true,
            dataObject: row,
            isCreating: false
        });
    };

    guardarEjercicio = () => {
        const {isCreating, dataObject} = this.state;

        if (isCreating) {
            crearEjercicio(dataObject)
                .then(data => {
                    const ejercicios = [...this.state.ejercicios];
                    ejercicios.push(data.Datos[0]);
                    this.setState({
                        ejercicios,
                        modalEjercicioOpen: false,
                        dataObject: {},
                        isCreating: false
                    });
                })
                .catch(err => {
                    console.log('err', err);
                })
        } else {
            actualizarEjercicio(dataObject)
                .then(data => {
                    this.setState({
                        modalEjercicioOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
        }
    }

    guardarPerdiodo = () => {
        const {isCreating, dataObject, ejercicioId} = this.state;

        if (isCreating) {
            crearPeriodo(ejercicioId, dataObject)
                .then(data => {
                    const state = {...this.state};
                    if (data.Exitoso) {
                        const obj = data.Datos[0];

                        const ejercicioIndex = state.ejercicios.findIndex(e => e.EjercicioOperativoID === ejercicioId);
                        if (ejercicioIndex >= 0) {
                            const ejercicio = state.ejercicios[ejercicioIndex];
                            ejercicio.PeriodosOperativos.push(obj);
                            console.log('eje', ejercicio);
                            console.log('isx', ejercicioIndex);
                            state.ejercicios[ejercicioIndex] = [
                                ...state.ejercicios.slice(0, ejercicioIndex),
                                ...[{...ejercicio}],
                                ...state.ejercicios.slice(ejercicioIndex + 1),
                            ];
                        }

                        state.modalPeriodoOpen = false;
                        state.dataObject = {};
                    }

                    this.setState(state, this.fetchData);
                });
        } else {
            actualizarPeriodo(dataObject)
                .then(data => {
                    const state = {...this.state};
                    if (data.Exitoso) {
                        const obj = data.Datos[0];

                        const ejercicioIndex = state.ejercicios.findIndex(e => e.EjercicioOperativoID === ejercicioId);
                        if (ejercicioIndex >= 0) {
                            const ejercicio = state.ejercicios[ejercicioIndex];
                            const periodoIndex = ejercicio.PeriodosOperativos.findIndex(p => p.PeriodoOperativoID === dataObject.PeriodoOperativoID);
                            if (periodoIndex >= 0) {
                                state.ejercicios[ejercicioIndex].PeriodosOperativos = [
                                    ...state.ejercicios[ejercicioIndex].PeriodosOperativos.slice(0, periodoIndex),
                                    ...[{...obj}],
                                    ...state.ejercicios[ejercicioIndex].PeriodosOperativos.slice(periodoIndex + 1),
                                ];
                            }
                        }

                        state.modalPeriodoOpen = false;
                        state.dataObject = {};
                    }

                    this.setState(state, this.fetchData);
                })
        }
    }

    onChangeDataObject = (field, {target: {value}}) => {
        const dataObject = {...this.state.dataObject};
        dataObject[field] = value;
        this.setState({dataObject});
    };

    renderModalEjercicio = () => (
        <Modal isOpen={this.state.modalEjercicioOpen} toggle={this.toggleModal.bind(this, 'modalEjercicioOpen')}>
            <ModalHeader toggle={this.toggle}>{this.state.isCreating ? 'Creando ejercicio' : 'Editando ejercicio'}</ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre corto</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.NombreCorto}
                        onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                        placeholder="nombre corto"
                        required
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Clave</Label>
                      <Input
                        type="text"
                        value={this.state.dataObject.EjercicioClave}
                        onChange={this.onChangeDataObject.bind(this, 'EjercicioClave')}
                        id="name"
                        placeholder="clave"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarEjercicio}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalEjercicioOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    onChangeDate = (field, date) => {
        const dataObject = {...this.state.dataObject};

        dataObject[field] = date;
        this.setState({dataObject});
    };

    editarPeriodo = periodo => {
        const dataObject = {...periodo};
        dataObject.FechaInicial = new Date(periodo.FechaInicial);
        dataObject.FechaFinal = new Date(periodo.FechaFinal);
        dataObject.FechaEjercePresupuesto = new Date(periodo.FechaEjercePresupuesto);

        this.setState({
            dataObject,
            modalPeriodoOpen: true,
            isCreating: false,
        })
    };

    renderModalPeriodo = () => (
        <Modal isOpen={this.state.modalPeriodoOpen} toggle={this.toggleModal.bind(this, 'modalPeriodoOpen')}>
            <ModalHeader toggle={this.toggle}>
                {this.state.isCreating ? 'Creando periodo' : 'Editando periodo'}
            </ModalHeader>
            <ModalBody>
            <Row>
              <Col xs="12" md="6">
                <FormGroup>
                  <Label htmlFor="ccmonth">Nombre</Label>
                  <Input
                    type="text"
                    id="name"
                    value={this.state.dataObject.Nombre}
                    onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                    placeholder="nombre"
                    required
                  />
                </FormGroup>
              </Col>
              <Col xs="12" md="6">
                <FormGroup>
                  <Label htmlFor="ccmonth">Orden</Label>
                  <Input
                    type="text"
                    value={this.state.dataObject.PeriodoOrden}
                    onChange={this.onChangeDataObject.bind(this, 'PeriodoOrden')}
                    id="name"
                    placeholder="clave"
                    required
                  />
                </FormGroup>
              </Col>
            </Row>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre corto</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.NombreCorto}
                        onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                        placeholder="nombre corto"
                        required
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Clave</Label>
                      <Input
                        type="text"
                        value={this.state.dataObject.PeriodoClave}
                        onChange={this.onChangeDataObject.bind(this, 'PeriodoClave')}
                        id="name"
                        placeholder="clave"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Fecha inicio</Label>
                      <DatePicker
                          defaultValue={this.state.dataObject.FechaInicial}
                          placeholder={'Fecha inicio'}
                          onChangeDate={this.onChangeDate.bind(this, 'FechaInicial')}
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                      <FormGroup>
                        <Label htmlFor="ccmonth">Fecha final</Label>
                        <DatePicker
                            defaultValue={this.state.dataObject.FechaFinal}
                            placeholder={'Fecha fin'}
                            onChangeDate={this.onChangeDate.bind(this, 'FechaFinal')}
                        />
                      </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6">
                      <FormGroup>
                        <Label htmlFor="ccmonth">Fecha ejerce presupuesto</Label>
                        <DatePicker
                            defaultValue={this.state.dataObject.FechaEjercePresupuesto}
                            placeholder={'Fecha ejerce presupuesto'}
                            onChangeDate={this.onChangeDate.bind(this, 'FechaEjercePresupuesto')}
                        />
                      </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarPerdiodo}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalPeriodoOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    expandRow = {
        renderer: row => {
            return (
                <div>
                    <Button
                        outline
                        color="primary"
                        onClick={this.editarEjercicio.bind(this, row)}>
                        <span className="fa fa-edit" style={{marginRight: 10}} />
                        Editar ejercicio
                    </Button>
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <h4>Periodos operativos</h4>
                        <Button
                            outline
                            color="primary"
                            onClick={this.agregarPeriodo.bind(this, row.EjercicioOperativoID)}>
                            <span className="fa fa-plus" />
                        </Button>
                    </div>
                    {row.PeriodosOperativos.map((periodo, indexPeriodo) => (
                        <div style={{flex: 1, margin: 3, display: 'inline-flex'}} key={indexPeriodo}>
                            <Badge pill color="primary">
                                {periodo.Nombre}
                                <span onClick={this.editarPeriodo.bind(this, periodo)} className="fa fa-edit" style={{marginLeft: 15, cursor: 'pointer'}} />
                                <span className="fa fa-remove" style={{marginLeft: 5, cursor: 'pointer'}} />
                            </Badge>
                        </div>
                    ))}
                </div>
            )
        }
    };

    obtenerEjerciciosColumnas = () => [
        {dataField: 'EjercicioOperativoID', text: 'ID'},
        {dataField: 'EjercicioClave', text: 'Clave'},
        {dataField: 'Nombre', text: 'Nombre'},
        {dataField: 'NombreCorto', text: 'Nombre Corto'},
        {dataField: '', text: ''},
    ];

    render = () => (
        <>
            <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 5}}>
                <Button
                    outline
                    color="primary"
                    onClick={this.agregarEjercicio}>
                    <span className="fa fa-plus" />
                </Button>
            </div>
            <TableCollapse
                data={this.state.ejercicios}
                columns={this.obtenerEjerciciosColumnas()}
                keyField={'EjercicioOperativoID'}
                expandRow={this.expandRow}
            />
            {this.renderModalEjercicio()}
            {this.renderModalPeriodo()}
        </>
    );
}




export default EjerciciosYPeriodos;
