import React from 'react';
import TableCollapse from '../../../components/tables/collapse';
import DatePicker from '../../../components/inputs/datePicker';
// import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import {getEjercicios} from '../../../services/presupuesto/ejercicios';
import {getTitulos, crearTitulo, actualizarTitulo} from '../../../services/presupuesto/titulos';
import {crearRubro, actualizarRubro} from '../../../services/presupuesto/rubros';
import {
    Badge,
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Col,
    FormGroup,
    Label,
    Input,
    Row,
 } from 'reactstrap';

class EjerciciosYPeriodos extends React.Component {
    state = {
        concept: 'I',
        modalTituloOpen: false,
        modalRubroOpen: false,
        ejercicioId: '',
        dataObject: {},
        ejercicios: [],
        titulos: [],
    }

    componentDidMount = () => {
        this.getExercises();
    }

    onChangeSelect = (field, {target: {value}}) => {
        this.setState({[field]: value}, this.getExercises);
    }

    getExercises = () => {
        getEjercicios()
            .then(data => {
                console.log('eje1', this.state.ejercicioId);
                let ejercicioId = this.state.ejercicioId;
                if (ejercicioId === '') {
                    ejercicioId = data.Datos[0].EjercicioOperativoID
                }
                console.log('eje', ejercicioId);
                this.setState({
                    ejercicios: data.Datos,
                    ejercicioId,
                }, this.fetchData);
            });
    };

    fetchData = () => {
        const {concept, ejercicioId} = this.state;
        getTitulos(concept, ejercicioId)
            .then(data => this.setState({titulos: data.Datos}));
    }

    toggleModal = field => {
        this.setState({[field]: !this.state[field]});
    }

    agregarTitulo = () => this.setState({
        modalTituloOpen: true,
        isCreating: true,
    });

    agregarRubro = titleId => this.setState({
        modalRubroOpen: true,
        isCreating: true,
        titleId,
    });

    editarTitulo = row => {
        this.setState({
            modalTituloOpen: true,
            dataObject: row,
            isCreating: false
        });
    };

    guardarTitulo = () => {
        const {isCreating, dataObject, ejercicioId, concept} = this.state;

        if (isCreating) {
            crearTitulo(ejercicioId, concept, dataObject)
                .then(data => {
                    const titulos = [...this.state.titulos];
                    titulos.push(data.Datos);
                    this.setState({
                        titulos,
                        modalTituloOpen: false,
                        dataObject: {},
                        isCreating: false
                    });
                })
                .catch(err => {
                    console.log('err', err);
                })
        } else {
            actualizarTitulo(dataObject)
                .then(data => {
                    this.setState({
                        modalTituloOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.getExercises);
                })
        }
    }

    guardarRubro = () => {
        const {isCreating, dataObject, titleId} = this.state;

        if (isCreating) {
            crearRubro(titleId, dataObject)
                .then(data => {
                    const state = {...this.state};
                    if (data.Exitoso) {
                        state.modalRubroOpen = false;
                        state.dataObject = {};
                    }

                    this.setState(state, this.fetchData);
                });
        } else {
            delete dataObject.CuentaContable;
            delete dataObject.CuentaContableID;
            actualizarRubro(dataObject)
                .then(data => {
                    const state = {...this.state};
                    if (data.Exitoso) {
                        state.modalRubroOpen = false;
                        state.dataObject = {};
                    }

                    this.setState(state, this.fetchData);
                })
        }
    }

    onChangeDataObject = (field, {target: {value}}) => {
        const dataObject = {...this.state.dataObject};
        dataObject[field] = value;
        this.setState({dataObject});
    };

    renderModalTitulo = () => (
        <Modal isOpen={this.state.modalTituloOpen} toggle={this.toggleModal.bind(this, 'modalTituloOpen')}>
            <ModalHeader toggle={this.toggle}>{this.state.isCreating ? 'Creando titulo' : 'Editando titulo'}</ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre corto</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.NombreCorto}
                        onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                        placeholder="nombre corto"
                        required
                      />
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Clave</Label>
                      <Input
                        type="text"
                        value={this.state.dataObject.TituloRubroClave}
                        onChange={this.onChangeDataObject.bind(this, 'TituloRubroClave')}
                        id="name"
                        placeholder="clave"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarTitulo}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalTituloOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    onChangeDate = (field, date) => {
        const dataObject = {...this.state.dataObject};

        dataObject[field] = date;
        this.setState({dataObject});
    };

    editarPeriodo = periodo => {
        const dataObject = {...periodo};
        dataObject.FechaInicial = new Date(periodo.FechaInicial);
        dataObject.FechaFinal = new Date(periodo.FechaFinal);
        dataObject.FechaEjercePresupuesto = new Date(periodo.FechaEjercePresupuesto);

        this.setState({
            dataObject,
            modalRubroOpen: true,
            isCreating: false,
        })
    };

    renderModalRubro = () => (
        <Modal isOpen={this.state.modalRubroOpen} toggle={this.toggleModal.bind(this, 'modalRubroOpen')}>
            <ModalHeader toggle={this.toggle}>
                {this.state.isCreating ? 'Creando rubro' : 'Editando rubro'}
            </ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="ccmonth">Nombre corto</Label>
                        <Input
                          type="text"
                          id="name"
                          value={this.state.dataObject.NombreCorto}
                          onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                          placeholder="nombre corto"
                          required
                        />
                      </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarRubro}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalRubroOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    expandRow = {
        renderer: row => {
            return (
                <div>
                    <Button
                        outline
                        color="primary"
                        onClick={this.editarTitulo.bind(this, row)}>
                        <span className="fa fa-edit" style={{marginRight: 10}} />
                        Editar titulo
                    </Button>
                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <h4>Rubros contables</h4>
                        <Button
                            outline
                            color="primary"
                            onClick={this.agregarRubro.bind(this, row.TituloRubroID)}>
                            <span className="fa fa-plus" />
                        </Button>
                    </div>
                    {row.RubrosContables.map((rubro, indexRubro) => (
                        <div style={{flex: 1, margin: 3, display: 'inline-flex'}} key={`rubro-${indexRubro}`}>
                            <Badge pill color="primary">
                                {rubro.Nombre}
                                <span onClick={this.editarPeriodo.bind(this, rubro)} className="fa fa-edit" style={{marginLeft: 15, cursor: 'pointer'}} />
                                <span className="fa fa-remove" style={{marginLeft: 5, cursor: 'pointer'}} />
                            </Badge>
                        </div>
                    ))}
                </div>
            )
        }
    };

    obtenerTituloColumnas = () => [
        {dataField: 'TituloRubroID', text: 'ID'},
        {dataField: 'TituloRubroClave', text: 'Clave'},
        {dataField: 'Nombre', text: 'Nombre'},
        {dataField: 'NombreCorto', text: 'Nombre Corto'},
    ];

    render = () => (
        <>
            <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 5}}>
                <Button
                    outline
                    color="primary"
                    onClick={this.agregarTitulo}>
                    <span className="fa fa-plus" />
                </Button>
            </div>
            <Row>
              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="exercise">Ejercicio</Label>
                  <Input type="select" name="exercise" id="exercise" value={this.state.ejercicioId} onChange={this.onChangeSelect.bind(this, 'ejercicioId')}>
                    <option value="0">Selecciona un ejercicio</option>
                    {this.state.ejercicios.map((ejercicio, index) => (
                        <option value={ejercicio.EjercicioOperativoID} key={`exercise-${index}`}>
                            {ejercicio.Nombre}
                        </option>
                    ))}
                  </Input>
                </FormGroup>
              </Col>
              <Col xs="6">
                <FormGroup>
                  <Label htmlFor="ccyear">Concepto</Label>
                  <Input
                    type="select"
                    name="concepto"
                    id="concepto"
                    value={this.state.concept}
                    onChange={this.onChangeSelect.bind(this, 'concept')}>
                    <option value="I">Ingreso</option>
                    <option value="E">Egreso</option>
                  </Input>
                </FormGroup>
              </Col>
            </Row>
            <TableCollapse
                data={this.state.titulos}
                columns={this.obtenerTituloColumnas()}
                keyField={'TituloRubroID'}
                expandRow={this.expandRow}
            />
            {this.renderModalTitulo()}
            {this.renderModalRubro()}
        </>
    );
}




export default EjerciciosYPeriodos;
