import React from 'react';
import TableCollapse from '../../../components/tables/collapse';
import {getConceptos, crearConcepto, actualizarConcepto} from '../../../services/presupuesto/conceptoIngreso';
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Col,
    FormGroup,
    Label,
    Input,
    Row,
 } from 'reactstrap';

class EjerciciosYPeriodos extends React.Component {
    state = {
        modalOpen: false,
        dataObject: {},
        conceptos: [],
    }

    componentDidMount = () => {
        this.fetchData();
    }

    fetchData = () => {
        getConceptos()
            .then(data => {
                this.setState({
                    conceptos: data.Datos
                });
            });
    };

    toggleModal = field => {
        this.setState({[field]: !this.state[field]});
    }

    agregarCentro = () => this.setState({
        modalOpen: true,
        isCreating: true,
    });

    agregarRubro = titleId => this.setState({
        modalRubroOpen: true,
        isCreating: true,
        titleId,
    });

    editarConcepto = row => {
        this.setState({
            modalOpen: true,
            dataObject: row,
            isCreating: false
        });
    };

    guardarConcepto = () => {
        const {isCreating, dataObject} = this.state;

        if (isCreating) {
            crearConcepto(dataObject)
                .then(data => {
                    const conceptos = [...this.state.conceptos];
                    conceptos.push(data.Datos);
                    this.setState({
                        conceptos,
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
                .catch(err => {
                    console.log('err', err);
                })
        } else {
            actualizarConcepto(dataObject)
                .then(data => {
                    this.setState({
                        modalOpen: false,
                        dataObject: {},
                        isCreating: false
                    }, this.fetchData);
                })
        }
    }

    onChangeDataObject = (field, {target: {value}}) => {
        const dataObject = {...this.state.dataObject};
        dataObject[field] = value;
        this.setState({dataObject});
    };

    renderModal = () => (
        <Modal isOpen={this.state.modalOpen} toggle={this.toggleModal.bind(this, 'modalOpen')}>
            <ModalHeader toggle={this.toggle}>{this.state.isCreating ? 'Creando Concepto de ingreso' : 'Editando concepto de ingreso'}</ModalHeader>
            <ModalBody>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="name">Nombre</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.Nombre}
                        onChange={this.onChangeDataObject.bind(this, 'Nombre')}
                        placeholder="nombre"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12">
                    <FormGroup>
                      <Label htmlFor="ccmonth">Nombre corto</Label>
                      <Input
                        type="text"
                        id="name"
                        value={this.state.dataObject.NombreCorto}
                        onChange={this.onChangeDataObject.bind(this, 'NombreCorto')}
                        placeholder="nombre corto"
                        required
                      />
                    </FormGroup>
                  </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={this.guardarConcepto}>Guardar</Button>{' '}
                <Button color="secondary" onClick={this.toggleModal.bind(this, 'modalOpen')}>Cancel</Button>
            </ModalFooter>
        </Modal>
    );

    addButtonLancamento = (cell, row) => {
        return (
            <Button
                outline
                color="primary"
                onClick={this.editarConcepto.bind(this, row)}>
                <span className="fa fa-edit" />
            </Button>
        )
    }

    obtenerTituloColumnas = () => [
        {dataField: 'ConceptoIngresoID', text: 'ID'},
        {dataField: 'Nombre', text: 'Nombre'},
        {dataField: 'NombreCorto', text: 'Nombre Corto'},
        {
            text: '',
            formatter: this.addButtonLancamento,
            editable: false
        }
    ];

    render = () => (
        <>
            <div style={{display: 'flex', justifyContent: 'flex-end', marginBottom: 5}}>
                <Button
                    outline
                    color="primary"
                    onClick={this.agregarCentro}>
                    <span className="fa fa-plus" />
                </Button>
            </div>
            <TableCollapse
                data={this.state.conceptos}
                columns={this.obtenerTituloColumnas()}
                keyField={'CentroCostoID'}
            />
            {this.renderModal()}
        </>
    );
}




export default EjerciciosYPeriodos;
