import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';

const getRowStyle = (row) =>{
    let backgroundColor = '';
    let color = '';
    if(row.TituloRubroID) {
        backgroundColor = '#3b5998';
        color = 'white';
    } else if(row.RubroContableID) {
        backgroundColor = '#20a8d8';
        color = 'white';
    }
    return {
        backgroundColor,
        color
    }
}
const getCellEdit =(beforeSaveCell) => cellEditFactory({
    mode: 'click',
    beforeSaveCell: beforeSaveCell
})

const PresupuestoSolicitadoTabla = ({columns, data, beforeSaveCell})=>{
    if(!data || data.length === 0) {
        return null;
    }
    return <BootstrapTable keyField="id" data={data} rowStyle={getRowStyle} columns={columns} cellEdit={getCellEdit(beforeSaveCell)}></BootstrapTable>
}
export default PresupuestoSolicitadoTabla;