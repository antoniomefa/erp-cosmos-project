import React from 'react';
import DatePicker from 'react-datepicker';

// docs
// https://reactdatepicker.com/#example-include-times

const Picker = ({defaultValue, placeholder, onChangeDate, ...rest}) => (
    <DatePicker
        selected={defaultValue}
        onChange={onChangeDate}
        className="form-control"
        dateFormat="dd/MM/yyyy"
        placeholderText={placeholder}
    />
);

export default Picker;
