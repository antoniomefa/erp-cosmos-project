import React from 'react';
import { Card, CardBody, Form, FormGroup, Label, Input, Row, Col, Button } from 'reactstrap';

function handleSubmit(e, handler) {
    e.preventDefault();
    const data = {
        IngresoOEgreso: e.target.elements.tipopresupuesto.value,
        EjercicioOperativoID: parseInt(e.target.elements.periodooperativo.value, 10),
        CentroCostoID: e.target.elements.centrocosto.value,
        EstatusDocumentoID: e.target.elements.estatusdocumento.value,
        EmpresaID: 1
    }
    handler(data);
}
const PresupuestoSolicitadoFiltros = (props) => {
    return (
        <Card>
            <CardBody>
                <Form onSubmit={e => handleSubmit(e, props.handleSubmit)}>
                    <Row className="allign-items-center">
                        <Col xs="1">
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" defaultChecked value="I" name="tipopresupuesto" />{' '}
                                    Ingresos
                                </Label>
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="radio" value="E" name="tipopresupuesto" />{' '}
                                    Egresos
                                </Label>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="periodooperativo">Ejercicio Operativo</Label>
                                <Col>
                                    <Input type="select" name="periodooperativo" id="periodooperativo">
                                        {
                                            props.ejercicios.map((e)=> <option key={e.EjercicioOperativoID} value={e.EjercicioOperativoID}>{e.Nombre}</option> )
                                        }
                                    </Input>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="centrocosto">Centro de costo</Label>
                                <Col>
                                    <Input type="select" name="centrocosto" id="centrocosto">
                                        <option value="1">Primaria</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col>
                            <FormGroup>
                                <Label for="estatusdocumento">Estatus Documento</Label>
                                <Col>
                                    <Input type="select" name="estatusdocumento" id="estatusdocumento">
                                        <option value="2">En Proceso</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                        </Col>
                        <Col xs="1">
                            <FormGroup row>
                                <Button>Desplegar</Button>
                            </FormGroup>
                        </Col>
                    </Row>
                </Form>
            </CardBody>
        </Card>
    );
}

export default PresupuestoSolicitadoFiltros;