import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Button, DropdownItem, DropdownMenu,  DropdownToggle, Nav, UncontrolledDropdown} from 'reactstrap';

class TableCollapse extends React.Component {

    ExportXLSX (csvData, fileName) {
        const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        const fileExtension = '.xlsx';
        const ws = XLSX.utils.json_to_sheet(csvData);
        const wb = { Sheets: { 'data': ws }, SheetNames: ['data'] };
        const excelBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
        const data = new Blob([excelBuffer], {type: fileType});
        FileSaver.saveAs(data, fileName + fileExtension);
    }

    ExportPDF (csvData, columns) {
        var doc = new jsPDF('p', 'pt', 'letter')
        doc.autoTable({ body: csvData})
        doc.save('archivo.pdf')
    }

    render(){
        const {data, columns, keyField, rowStyle, tableHeaderClass, ...rest} = this.props;
        return (

            <ToolkitProvider
            keyField={keyField}
            data={ data }
            columns={ columns }
            exportCSV={ {
                fileName: 'archivo.csv',
                exportAll: true
              } }
            >
            {
                props => (
                <div>
                    <Nav >
                        <UncontrolledDropdown nav direction="right">
                            <DropdownToggle caret>
                                <span className="fa fa-arrow-circle-down"/> Exportar
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem 
                                    { ...props.csvProps }
                                    onClick={ ()=> this.ExportXLSX(data,'archivo') }
                                >
                                    <i className="fa fa-file-excel-o"></i> 
                                    Excel
                                </DropdownItem>
                                <DropdownItem  
                                    { ...props.csvProps }
                                    onClick={ ()=> props.csvProps.onExport() }
                                >
                                    <i className="fa fa-table"></i> 
                                    CSV
                                </DropdownItem>
                                <DropdownItem  
                                    { ...props.csvProps }
                                    onClick={ ()=> this.ExportPDF(data, columns) }
                                >
                                    <i className="fa fa-file-pdf-o"></i> 
                                    PDF
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>    

                    <BootstrapTable 
                        className="MyTable"
                        keyField={keyField}
                        tableHeaderClass={tableHeaderClass}
                        rowStyle={rowStyle}
                        remote={{ sort: true }}
                        data={data}
                        columns={columns}
                        {...rest}
                        { ...props.baseProps }
                    />
                    
                </div>
                )
            }
            </ToolkitProvider>
         
        );
    }
}

TableCollapse.defaultProps = {
    data: [],
    keyField: 'id',
    columns: [],
    onTableChange: () => {}
}

export default TableCollapse;