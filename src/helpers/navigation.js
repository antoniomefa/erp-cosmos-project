export default {
  items: [
    {
      title: false,
      name: 'Menu',
      wrapper: {            // optional wrapper object
        element: '',        // required valid HTML5 element tag
        attributes: {}      // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: ''             // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-home',
      badge: {
        variant: 'info',
        text: '1',
      },
    },
    {
      name: 'Usuarios',
      url: '/base',
      icon: 'icon-settings',
      children: [
        {
          name: 'Cuentas de usuarios',
          url: '/usuarios/cuentas-usuarios',
        },
        {
          name: 'Roles por usuario',
          url: '/usuarios/roles-usuario',
        },
      ],
    },
    {
      name: 'Empresas',
      url: '/base',
      icon: 'icon-briefcase',
      children: [
        {
          name: 'Empresas',
          url: '/empresas/empresas',
        },
        {
          name: 'Zonas',
          url: '/empresas/zonas',
        },
        {
          name: 'Catálogo de empleados',
          url: '/empresas/catalogo-empleados',
        },
        {
          name: 'Cuota sindical por semana',
          url: '/empresas/cuota-sindical',
        },
      ],
    },
    {
      name: 'Caja de ahorro',
      url: '/base',
      icon: 'icon-wallet',
      children: [
        {
          name: 'Aportaciones a caja',
          url: '/caja/aportaciones',
        },
        {
          name: 'Reglas de prestamos',
          url: '/caja/reglas',
        },
        {
          name: 'Prestamos',
          url: '/caja/prestamos',
        },
        {
          name: 'Consulta aportaciones x empleado',
          url: '/caja/consulta-aportaciones',
        },
        {
          name: 'Aprobar movimientos',
          url: '/caja/movimientos',
        },
        {
          name: 'Robot asignar prestamos',
          url: '/caja/asignar-prestamos',
        },
      ],
    },
    {
      name: 'Reportes',
      url: '/base',
      icon: 'icon-graph',
      children: [
        {
          name: 'reportes de ahorro',
          url: '/reportes/ahorro',
        },
      ],
    },
    {
      divider: true,
    },
    //{
    //  name: 'Cerrar sesión',
    //  url: '/base',
    //  icon: 'icon-logout',
    //  class: 'mt-auto',
    //  variant: 'info',
    //  attributes: { onClick= {
    //    () => onLogout(e)
    //   },
    //  },
    //},
  ],
};
