import React from 'react';

// dashboard
const Dashboard = React.lazy(() => import('../containers/dashboard'));

// Usuarios
const DashboardUsuarios = React.lazy(() => import('../containers/Usuarios/dashboard'));
const CuentasUsuarios = React.lazy(() => import('../containers/Usuarios/componentes/cuentasUsuarios'));
const RolesPorUsuario = React.lazy(() => import('../containers/Usuarios/componentes/rolesUsuario'));

// empresas
const DashboardEmpresas = React.lazy(() => import('../containers/Empresas/dashboard'));
const Empresas = React.lazy(() => import('../containers/Empresas/componentes/empresas'));
const Zonas = React.lazy(() => import('../containers/Empresas/componentes/zonas'));
const CatalogoEmpleados = React.lazy(() => import('../containers/Empresas/componentes/catalogoEmpleados'));
const CuotaSindical = React.lazy(() => import('../containers/Empresas/componentes/cuotaSindical'));

// caja de ahorro
const DashboardCaja = React.lazy(() => import('../containers/CajaDeAhorro/dashboard'));
const Aportaciones = React.lazy(() => import('../containers/CajaDeAhorro/componentes/aportaciones'));
const Reglas = React.lazy(() => import('../containers/CajaDeAhorro/componentes/reglas'));
const Prestamos = React.lazy(() => import('../containers/CajaDeAhorro/componentes/prestamos'));
const ConsultaAportaciones = React.lazy(() => import('../containers/CajaDeAhorro/componentes/consultaAportaciones'));
const Movimientos = React.lazy(() => import('../containers/CajaDeAhorro/componentes/movimientos'));
const AsignarPrestamos = React.lazy(() => import('../containers/CajaDeAhorro/componentes/asignarPrestamos'));

// reportes
const DashboardReportes = React.lazy(() => import('../containers/Reportes/dashboard'));
const ReportesAhorro = React.lazy(() => import('../containers/Reportes/componentes/reporteAhorro'));

const routes = [
    { path: '/', exact: true, name: '', component: Dashboard },

    { path: '/usuarios', exact: true, name: 'Usuarios', component: DashboardUsuarios },
    { path: '/usuarios/cuentas-usuarios', exact: true, name: 'Cuentas de usuarios', component: CuentasUsuarios },
    { path: '/usuarios/roles-usuario', exact: true, name: 'Roles por usuario', component: RolesPorUsuario },

    { path: '/empresas', exact: true, name: 'Empresas', component: DashboardEmpresas },
    { path: '/empresas/empresas', exact: true, name: 'Empresas', component: Empresas },
    { path: '/empresas/zonas', exact: true, name: 'Zonas', component: Zonas },
    { path: '/empresas/catalogo-empleados', exact: true, name: 'Catálogo de empleados', component: CatalogoEmpleados },
    { path: '/empresas/cuota-sindical', exact: true, name: 'Cuota sindical', component: CuotaSindical },

    { path: '/caja', exact: true, name: 'Caja de ahorros', component: DashboardCaja },
    { path: '/caja/aportaciones', exact: true, name: 'Aportaciones', component: Aportaciones },
    { path: '/caja/reglas', exact: true, name: 'Reglas de prestamos', component: Reglas },
    { path: '/caja/prestamos', exact: true, name: 'Prestamos', component: Prestamos },
    { path: '/caja/consulta-aportaciones', exact: true, name: 'Consulta aportaciones x empleado', component: ConsultaAportaciones },
    { path: '/caja/movimientos', exact: true, name: 'Aprobar movimientos', component: Movimientos },
    { path: '/caja/asignar-prestamos', exact: true, name: 'Asignar prestamos', component: AsignarPrestamos },

    { path: '/reportes', exact: true, name: 'Reportes', component: DashboardReportes },
    { path: '/reportes/ahorro', exact: true, name: 'Reportes de ahorro', component: ReportesAhorro },
];

export default routes;
